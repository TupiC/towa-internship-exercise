import React from "react";
import { Popover } from "@headlessui/react";
import { Bars3Icon } from "@heroicons/react/24/outline";

export function DesktopHeader({ Logo, setMobileMenuOpen }) {
    return (
        <nav
            className="flex items-center justify-between p-6 lg:px-8"
            aria-label="Global"
        >
            <div className="flex lg:flex-1">
                <a href="#main" className="-m-1.5 p-1.5">
                    <span className="sr-only">Pokemon TOWA</span>
                    <img className="h-8 w-auto" src={Logo} alt="" />
                </a>
            </div>
            <div className="flex lg:hidden">
                <button
                    type="button"
                    className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700"
                    onClick={() => setMobileMenuOpen(true)}
                >
                    <span className="sr-only">Open main menu</span>
                    <Bars3Icon
                        className="h-6 w-6 text-white"
                        aria-hidden="true"
                    />
                </button>
            </div>
            <Popover.Group className="hidden lg:flex lg:gap-x-12">
                <a
                    href="#feature1"
                    className="text-sm font-semibold leading-6 text-white"
                >
                    Feature1
                </a>
                <a
                    href="#feature2"
                    className="text-sm font-semibold leading-6 text-white"
                >
                    Feature2
                </a>
                <a
                    href="#feature3"
                    className="text-sm font-semibold leading-6 text-white"
                >
                    Feature3
                </a>
            </Popover.Group>
        </nav>
    );
}
