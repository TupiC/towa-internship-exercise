import { DesktopHeader } from "./DesktopHeader";
import { MobileHeader } from "./MobileHeader";
import { useState } from "react";
import Logo from "../../assets/img/logo.svg";

export const Header = () => {
    const [mobileMenuOpen, setMobileMenuOpen] = useState(false);

    return (
        <header className="bg-gray-400 dark:bg-zinc-800">
            <DesktopHeader Logo={Logo} setMobileMenuOpen={setMobileMenuOpen} />
            <MobileHeader
                mobileMenuOpen={mobileMenuOpen}
                setMobileMenuOpen={setMobileMenuOpen}
                Logo={Logo}
            />
        </header>
    );
};
