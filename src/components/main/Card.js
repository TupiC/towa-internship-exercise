import { ImageWithLoading } from './ImageWithLoading';
import { PokemonDescription } from "./PokemonDescription";
import React, { useState, useEffect } from "react";

export const Card = ({ pokemonUrl }) => {
    const [pokemonData, setPokemonData] = useState();
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const dataFetch = async () => {
            let data = await (await fetch(pokemonUrl)).json();

            setPokemonData(data);
            setIsLoading(false);
        };
        dataFetch();
    });

    if (isLoading) return null
    
    const pokemonName =
        pokemonData.name.charAt(0).toUpperCase() + pokemonData.name.slice(1);
    return (
        <a
            href="https://www.towa-digital.com/"
            target="_blank"
            className="block rounded-lg bg-white shadow-[4px_4px_8px_-3px_rgba(0,0,0,0.7),0_10px_20px_-2px_rgba(0,0,0,0.04)] hover:scale-105 transition dark:bg-neutral-700 max-w-sm flex flex-col items-center"
            rel="noreferrer"
        >
            <ImageWithLoading  src={pokemonData.sprites.front_default} />
            <div className="p-6">
                <h5 className="mb-2 text-xl font-medium leading-tight text-neutral-800 dark:text-neutral-50">
                    {pokemonName}
                </h5>
                <PokemonDescription
                    pokemonName={pokemonName}
                    pokemonData={pokemonData}
                />
            </div>
        </a>
    );
};
