import React, { useState, useEffect } from "react";
import { Card } from "./Card";
import { useFormik } from "formik";
import Fuse from "fuse.js";
import { Loader } from "../Loader";

export const Main = () => {
    const [data, setData] = useState([]);
    const [searchedData, setSearchedData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    const options = {
        includeScore: true,
        keys: ["name"],
    };

    const fuse = new Fuse(data, options);

    const formik = useFormik({
        initialValues: { search: "" },
        onChange: () => {
            console.log(formik.values);
        },
    });

    const handleInputChange = (event) => {
            let searchTerm = event.target.value;
        formik.handleChange(event);
        
            if (searchTerm.length >= 3) {
                setSearchedData(fuse.search(searchTerm).map((obj) => obj.item));
            } else {
                setSearchedData(data);
            }
    };

    useEffect(() => {
        const dataFetch = async () => {
            const data = await (
                await fetch(`https://pokeapi.co/api/v2/pokemon?limit=30`)
            ).json();

            setData(data.results);
            setSearchedData(data.results);
            setIsLoading(false);
        };

        dataFetch();
    }, []);

    if (isLoading) return <Loader />;

    return (
        <div className="flex flex-col justify-center items-center bg-gray-200 dark:bg-neutral-400">
            <h1 className="text-5xl font-semibold my-8">Pokemon TOWA</h1>

            <div className="bg-gray-100 rounded border-2 border-gray-400 flex items-center rounded-lg">
                <input
                    type="text"
                    id="search"
                    name="search"
                    placeholder="Search pokemons..."
                    onChange={handleInputChange}
                    value={formik.values.search}
                    className="py-2 text-gray-600 px-4 focus:outline-none w-full rounded-lg"
                />
            </div>

            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 mx-8 md:mx-16 lg:mx32 justify-items-center max-w-max my-8 gap-4">
                {searchedData.map((data) => (
                    <Card key={data.name} pokemonUrl={data.url}></Card>
                ))}
            </div>
        </div>
    );
};
