import React, { useState } from "react";
import { Loader } from "../Loader";

export const ImageWithLoading = ({ src }) => {
    const [isLoading, setIsLoading] = useState(true);

    const handleLoad = () => {
        setIsLoading(false);
    };

    const handleError = () => {
        setIsLoading(false);
    };

    return (
        <div>
            {isLoading && <Loader />}
            <img
                src={src}
                onLoad={handleLoad}
                onError={handleError}
                className="rounded-t-lg max-w-sm"
                style={{ display: isLoading ? "none" : "block" }}
                alt="pokemon"
            />
        </div>
    );
};

export default ImageWithLoading;
