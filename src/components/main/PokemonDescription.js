import React from "react";

export function PokemonDescription({ pokemonName, pokemonData }) {
    const description = `${pokemonName} has the type${pokemonData.types.length > 1 ? "s" : ""} 
        ${pokemonData.types.map((type) => type.type.name).join(" and ")}.
        It weights ${pokemonData.weight} pounds.`
    
    return (
        <p
            className="mb-4 text-base text-neutral-600 dark:text-neutral-200"
            style={{
                display: "-webkit-box",
                WebkitLineClamp: 2,
                WebkitBoxOrient: "vertical",
                overflow: "hidden",
                textOverflow: "ellipsis",
            }}
        >
            {description}
        </p>
    );
}
