import { Links } from './Links';
import { Contact } from './Contact';
import React from "react";
import Logo from '../../assets/img/logo.svg'
export const Footer = () => {
    return (
        <footer className="bg-neutral-100 text-center text-neutral-600 dark:bg-zinc-700 dark:text-neutral-200 lg:text-left">
    
            <div className="mx-6 py-10 text-center md:text-left">
                <div className="grid-1 grid gap-8 md:grid-cols-2 lg:grid-cols-3 justify-items-center items-center">
                    <div className="flex-column font-semibold uppercase">
                            <img className="h-32 w-auto" src={Logo} alt="" />
                    </div>
                   <Links     />
                    <Contact />
                </div>
            </div>

            <div className="bg-neutral-200 p-6 text-center dark:bg-zinc-800">
                <span>© 2023 Copyright: </span>
                <a
                    className="font-semibold text-neutral-600 dark:text-neutral-400"
                    href="https://tailwind-elements.com/"
                >
                    Pokemon TOWA
                </a>
            </div>
        </footer>
    );
};
