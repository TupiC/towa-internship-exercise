import React from "react";
export function Links() {
    return (
        <div className="">
            <p className="mb-4">
                <a
                    href="#feature1"
                    className="text-neutral-600 dark:text-neutral-200 text-2xl"
                >
                    Feature1
                </a>
            </p>
            <p className="mb-4">
                <a
                    href="#feature2"
                    className="text-neutral-600 dark:text-neutral-200 text-2xl"
                >
                    Feature2
                </a>
            </p>
            <p className="">
                <a
                    href="#feature3"
                    className="text-neutral-600 dark:text-neutral-200 text-2xl"
                >
                    Feature3
                </a>
            </p>
        </div>
    );
}
